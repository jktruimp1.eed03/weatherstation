# weatherStation
**程式介紹**
* 包含城市名稱的輸入框
* 輸入城市名稱後顯示該地即時天氣狀況(溫度,天氣)
* 地點若找不到會發alert告訴使用者
* 可查詢已搜尋過的資料,包含搜尋時間,地點,天氣狀況
* 利用cookie紀錄前五筆搜尋過的資料
* 使用的天氣api: https://openweathermap.org/current


**環境安裝**

* 先裝虛擬環境 
`pip3 install virtualenv`

* 成功安裝完後,創立虛擬環境 
`virtualenv env`

* 進入虛擬環境 
`source env/bin/activate`

* 進入後下載requirements.txt上的所需套件 
`pip3 insall -r requirements.txt`

* 然後進入專案 
`cd weatherstation/weatherStation`

* 先migrate 
```
python3 manage.py makemigrations
python3 manage.py migrate
```


* 執行 
`python3 manage.py runserver`

* 搜尋頁面網址  
[http://127.0.0.1:8000/door/index/](http://127.0.0.1:8000/door/index/)

* 查詢table網址 
[http://127.0.0.1:8000/door/weatherTable/](http://127.0.0.1:8000/door/weatherTable/)

* Demo 影片： 
[Demo影片](https://drive.google.com/file/d/1-2dDrPDM2HE2slBS5ZOaJQS4jphS-2Y7/view?usp=sharing)