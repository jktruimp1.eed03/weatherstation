from django.db import models
from datetime import datetime
# Create your models here.
class Record(models.Model):
    id = models.AutoField(primary_key=True)
    searchTime = models.DateTimeField(default=datetime.now)
    weather = models.CharField(max_length=256,default=None,blank=True, null=True)
    cityName = models.CharField(max_length=256,default=None,blank=True, null=True)
    degree = models.FloatField(default=0.0, blank=True, null=True)
    country = models.CharField(max_length=256,default=None, blank=True, null=True)