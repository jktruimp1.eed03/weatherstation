from django.urls import path,include
from . import views
app_name='door'
urlpatterns = [
    path('index/', views.index , name='index'),
    path('weatherTable/', views.weatherTable , name='weatherTable'),
    path('setCookie/<str:key>/<str:value>', views.setCookie , name='setCookie'),
    path('getCookie/<str:key>', views.getCookie , name='getCookie'),
    path('getAllCookies/', views.getAllCookies , name='getAllCookies'),

]