from django.shortcuts import render, redirect, get_object_or_404


from api.models import *
import requests, json,datetime
from django.conf import settings
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger  # pagination
###gabor
def setDefaulCookies(request):
    response = render(request, 'index.html')
    tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
    tomorrow = datetime.datetime.replace(tomorrow, hour=0, minute=0, second=0)
    expires = datetime.datetime.strftime(tomorrow, "%a, %d-%b-%Y %H:%M:%S GMT")

    response.set_cookie("4", request.COOKIES.get("3", None), expires=expires)
    response.set_cookie("3", request.COOKIES.get("2", None), expires=expires)
    response.set_cookie("2", request.COOKIES.get("1", None), expires=expires)
    response.set_cookie("1", request.COOKIES.get("0", None), expires=expires)
    response.set_cookie("0", request.COOKIES.get("5", None), expires=expires)
    return response
def sortCookies(request):
    #01234
    response = render(request, 'index.html')
    tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
    tomorrow = datetime.datetime.replace(tomorrow, hour=0, minute=0, second=0)
    expires = datetime.datetime.strftime(tomorrow, "%a, %d-%b-%Y %H:%M:%S GMT")
    if "5" in request.COOKIES.keys():
        response.set_cookie("4", request.COOKIES.get("3", None), expires=expires)
        response.set_cookie("3", request.COOKIES.get("2", None), expires=expires)
        response.set_cookie("2", request.COOKIES.get("1", None), expires=expires)
        response.set_cookie("1", request.COOKIES.get("0", None), expires=expires)
        response.set_cookie("0", request.COOKIES.get("5", None), expires=expires)
        response.delete_cookie('5')
    return response

# Create your views here.
def getPagedRecords(request):
    limit = 8
    records = Record.objects.all().order_by('-searchTime')
    paginator = Paginator(records, limit)

    page = request.GET.get('page')
    try:
        records = paginator.page(page)
    except PageNotAnInteger:
        records = paginator.page(1)
    except EmptyPage:
        records = paginator.page(paginator.num_pages)

    return records
def index_t(request):
    response = render(request, 'index.html')
    response.set_cookie('my_cookie', 'Here is my cookie!')
    return response

def index(request):
    query = Record.objects.all().order_by('-searchTime')[:5]
    context={"query":query,"cookie0":request.COOKIES.get("0", None),"cookie1":request.COOKIES.get("1", None),
                      "cookie2":request.COOKIES.get("2", None),"cookie3":request.COOKIES.get("3", None),"cookie4":request.COOKIES.get("4", None)}
    #sortCookies(request)
    #context={"cookie0":request.COOKIES['0'],"cookie1":request.COOKIES['1'],"cookie2":request.COOKIES['2'],"cookie3":request.COOKIES['3'],"cookie4":request.COOKIES['4']}
    if request.method == "POST":
        allData = request.POST
        cityName = allData['cityName']
        url = settings.API_URL+cityName+"&appid="+settings.APP_ID
        print('request url: '+url)
        request_api = requests.get(url)
        if request_api.status_code == 200:
            json_data = json.loads(request_api.text)
            cityName = json_data['name']
            country = json_data['sys']['country']
            degree = round(float(json_data['main']['temp']) - 273.15,2)
            weather = json_data['weather'][0]['main']
            record = Record.objects.create(cityName=cityName, country=country,weather=weather, degree=degree)
            context = {'record': record, "query": query,"cookie0":record.cityName,"cookie1":request.COOKIES.get("0", None),
                      "cookie2":request.COOKIES.get("1", None),"cookie3":request.COOKIES.get("2", None),"cookie4":request.COOKIES.get("3", None)}
            print(request_api.text)
            #setCookie(request,str(record.id), record.cityName)
            tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
            tomorrow = datetime.datetime.replace(tomorrow, hour=0, minute=0, second=0)
            expires = datetime.datetime.strftime(tomorrow, "%a, %d-%b-%Y %H:%M:%S GMT")

            response = render(request,'index.html',context)


            response.set_cookie("4", request.COOKIES.get("3", None), expires=expires)
            response.set_cookie("3", request.COOKIES.get("2", None), expires=expires)
            response.set_cookie("2", request.COOKIES.get("1", None), expires=expires)
            response.set_cookie("1", request.COOKIES.get("0", None), expires=expires)
            response.set_cookie("0", record.cityName, expires=expires)
            #context= {'record': record, "query": query,"cookie0":record.cityName,"cookie1":request.COOKIES.get("0", None),"cookie2":request.COOKIES.get("1", None),"cookie3":request.COOKIES.get("2", None),"cookie4":request.COOKIES.get("3", None)}
            #response = render(request, 'index.html', context)
            return response


        else:
            print('not found')
            context = {'noResult': 'result not found, please input again!',"query":query,"query":query,"cookie0":request.COOKIES.get("0", None),"cookie1":request.COOKIES.get("1", None),
                      "cookie2":request.COOKIES.get("2", None),"cookie3":request.COOKIES.get("3", None),"cookie4":request.COOKIES.get("4", None)}

    return render(request, 'index.html',context)
@login_required
def weatherTable(request):
    records = getPagedRecords(request)
    context = {'records':records}
    return render(request,'weatherTable.html',context)


def setCookie(request, key=None, value=None):  # 定義set_cookie函式，key為名稱，value為內容
    tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
    tomorrow = datetime.datetime.replace(tomorrow, hour=0, minute=0, second=0)
    expires = datetime.datetime.strftime(tomorrow, "%a, %d-%b-%Y %H:%M:%S GMT")
    response = HttpResponse('Cookie 儲存完畢')
    response.set_cookie(key, value,expires=expires)
    print('cookie: '+str(getCookie(request,key)))
    return response


def getCookie(request, key=None):
    if key in request.COOKIES:  # 檢查指定的Cookie是否存在
        return HttpResponse('%s:%s' % (key, request.COOKIES[key]))
        #return str(request.COOKIES[key])
    else:
        return HttpResponse('Cookie 不存在!')
def getAllCookies(request):
    if request.COOKIES != None:
        strcookies=""
        for key1,value1 in request.COOKIES.items():
            strcookies= strcookies + key1 + ":" + value1 + "<br>"
        return HttpResponse("%s" % (strcookies))
    else:
        return HttpResponse('Cookie 不存在!')
###avl
